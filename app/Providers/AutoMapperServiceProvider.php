<?php

namespace App\Providers;

use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\AutoMapperInterface;
use BookStore\Infrastructure\AutoMapper\Config\MapperConfig;
use Illuminate\Support\ServiceProvider;

class AutoMapperServiceProvider extends ServiceProvider
{

    /**
     *
     */
    public function register()
    {
        $this->app->singleton(
            AutoMapperInterface::class,
            function () {
                return new AutoMapper(new MapperConfig());
            }
        );

    }//end register()

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }//end boot()

}//end class