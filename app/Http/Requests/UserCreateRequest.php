<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'             => 'required|email|max:255|unique:User',
            'password'          => 'required|string|confirmed|min:6',
            'firstname'         => 'required|string',
            'lastname'          => 'required|string',
            'dni'               => 'required|string'
        ];
    }//end rules()

}//end class
