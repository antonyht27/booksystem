<?php


namespace BookStore\Infrastructure\Repositories\Book;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use BookStore\Domain\Model\Book\Book;
use BookStore\Domain\Repositories\Book\BookRepository;



class DoctrineBookRepository extends EntityRepository implements BookRepository
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * DoctrineBookRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct($entityManager, $entityManager->getClassMetadata(Book::class));

    }

    /**
     * @param Book $book
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Book $book): void
    {
        $this->entityManager->persist($book);
        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @return Book|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function findOneById(int $id): ? Book
    {
        return $this->entityManager->find(Book::class,$id);
    }

}
