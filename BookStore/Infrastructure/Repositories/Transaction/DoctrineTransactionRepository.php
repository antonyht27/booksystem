<?php


namespace BookStore\Infrastructure\Repositories\Transaction;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use BookStore\Domain\Model\Transaction\Transaction;
use BookStore\Domain\Repositories\Transaction\TransactionRepository;



class DoctrineTransactionRepository extends EntityRepository implements TransactionRepository
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * DoctrineTransactionRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct($entityManager, $entityManager->getClassMetadata(Transaction::class));

    }

    /**
     * @param Transaction $transaction
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Transaction $transaction): void
    {
        $this->entityManager->persist($transaction);
        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @return Transaction|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function findOneById(int $id): ?Transaction
    {
        return $this->entityManager->find(Transaction::class,$id);
    }

}
