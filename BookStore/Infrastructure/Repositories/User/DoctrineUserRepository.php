<?php


namespace BookStore\Infrastructure\Repositories\User;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use BookStore\Domain\Model\User\User;
use BookStore\Domain\Repositories\User\UserRepository;



class DoctrineUserRepository extends EntityRepository implements UserRepository
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * DoctrineUserRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct($entityManager, $entityManager->getClassMetadata(User::class));

    }

    /**
     * @param User $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(User $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @return User|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function findOneById(int $id): ?User
    {
        return $this->entityManager->find(User::class,$id);
    }

}
