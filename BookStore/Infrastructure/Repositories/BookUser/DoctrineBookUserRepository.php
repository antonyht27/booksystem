<?php


namespace BookStore\Infrastructure\Repositories\BookUser;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use BookStore\Domain\Model\BookUser\BookUser;
use BookStore\Domain\Repositories\BookUser\BookUserRepository;



class DoctrineBookUserRepository extends EntityRepository implements BookUserRepository
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * DoctrineBookUserRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct($entityManager, $entityManager->getClassMetadata(BookUser::class));

    }

    /**
     * @param BookUser $bookUser
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(BookUser $bookUser): void
    {
        $this->entityManager->persist($bookUser);
        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @return BookUser|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function findOneById(int $id): ? BookUser
    {
        return $this->entityManager->find(BookUser::class,$id);
    }

}
