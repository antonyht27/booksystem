<?php


namespace BookStore\Infrastructure\Repositories\Promotion;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use BookStore\Domain\Model\Promotion\Promotion;
use BookStore\Domain\Repositories\Promotion\PromotionRepository;



class DoctrinePromotionRepository extends EntityRepository implements PromotionRepository
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * DoctrinePromotionRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct($entityManager, $entityManager->getClassMetadata(Promotion::class));

    }

    /**
     * @param Promotion $promotion
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Promotion $promotion): void
    {
        $this->entityManager->persist($promotion);
        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @return Promotion|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function findOneById(int $id): ?Promotion
    {
        return $this->entityManager->find(Promotion::class,$id);
    }

}
