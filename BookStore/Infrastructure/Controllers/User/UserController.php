<?php

namespace BookStore\Infrastructure\Controllers\User;

use Illuminate\Http\Request;
use BookStore\Domain\Dto\User\UserDto;
use AutoMapperPlus\AutoMapperInterface;
use App\Http\Requests\UserCreateRequest;
use BookStore\Application\Http\Controller;
use BookStore\Application\Services\User\RegisterService;
use BookStore\Infrastructure\Repositories\User\DoctrineUserRepository;

class UserController extends Controller
{
    /**
     * @var DoctrineUserRepository
     */
	protected $doctryneUserRepository;

    /**
     * @var AutoMapperInterface
     */
	protected $autoMapper;

    /**
     * UserController constructor.
     * @param DoctrineUserRepository $doctryneUserRepository
     * @param AutoMapperInterface $autoMapper
     */
    public function __construct(DoctrineUserRepository $doctryneUserRepository , AutoMapperInterface $autoMapper)
    {
    	$this->doctryneUserRepository = $doctryneUserRepository;
    	$this->autoMapper             = $autoMapper;
    }

    /**
     * @param UserCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \AutoMapperPlus\Exception\UnregisteredMappingException
     */
    public function store(UserCreateRequest $request)
    {
        $data = $this->autoMapper->map($request->all(), UserDto::class);

        $user = (new RegisterService($this->doctryneUserRepository))->execute($data);

        return response()->json($this->autoMapper->map($user, UserDto::class),201);
    }

}

