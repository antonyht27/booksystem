<?php

namespace BookStore\Infrastructure\Controllers\Promotion;

use Illuminate\Http\Request;
use AutoMapperPlus\AutoMapperInterface;
use BookStore\Application\Http\Controller;
use App\Http\Requests\PromotionCreateRequest;
use BookStore\Domain\Dto\Promotion\PromotionDto;
use BookStore\Application\Services\Promotion\RegisterService;
use BookStore\Infrastructure\Repositories\Book\DoctrineBookRepository;
use BookStore\Infrastructure\Repositories\Promotion\DoctrinePromotionRepository;

class PromotionController extends Controller
{
    /**
     * @var DoctrineBookRepository
     */
    protected $doctrineBookRepository;

    /**
     * @var AutoMapperInterface
     */
	protected $autoMapper;

    /**
     * PromotionController constructor.
     * @param DoctrinePromotionRepository $doctrynePromotionRepository
     * @param DoctrineBookRepository $doctrineBookRepository
     * @param AutoMapperInterface $autoMapper
     */
    public function __construct(DoctrinePromotionRepository $doctrynePromotionRepository , DoctrineBookRepository $doctrineBookRepository, AutoMapperInterface $autoMapper)
    {
    	$this->doctrynePromotionRepository = $doctrynePromotionRepository;
        $this->doctrineBookRepository = $doctrineBookRepository;
    	$this->autoMapper                  = $autoMapper;
    }

    /**
     * @param PromotionCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \AutoMapperPlus\Exception\UnregisteredMappingException
     * @throws \BookStore\Domain\Exception\RegisterPromotionException
     */
    public function store(PromotionCreateRequest $request)
    {
        $data = $this->autoMapper->map($request->all(), PromotionDto::class);

        $promotion = (new RegisterService($this->doctrynePromotionRepository,$this->doctrineBookRepository))->execute($data);

        return response()->json($this->autoMapper->map($promotion, PromotionDto::class),201);
    }

}

