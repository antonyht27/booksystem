<?php

namespace BookStore\Infrastructure\Controllers\Book;

use BookStore\Application\Services\Book\FinancingService;
use Illuminate\Http\Request;
use BookStore\Domain\Dto\Book\BookDto;
use AutoMapperPlus\AutoMapperInterface;
use App\Http\Requests\BookCreateRequest;
use BookStore\Application\Http\Controller;
use BookStore\Application\Services\Book\RegisterService;
use BookStore\Application\Services\Book\BuyingService;
use BookStore\Application\Services\Book\ReservingService;
use BookStore\Infrastructure\Repositories\Book\DoctrineBookRepository;
use BookStore\Infrastructure\Repositories\User\DoctrineUserRepository;
use BookStore\Infrastructure\Repositories\BookUser\DoctrineBookUserRepository;

class BookController extends Controller
{
    /**
     * @var DoctrineBookRepository
     */
	protected $doctryneBookRepository;

    /**
     * @var DoctrineUserRepository
     */
    protected $doctrineUserRepository;

    /**
     * @var DoctrineBookUserRepository
     */
    protected $doctrineBookUserRepository;

    /**
     * @var AutoMapperInterface
     */
	protected $autoMapper;

    /**
     * BookController constructor.
     * @param DoctrineBookRepository $doctryneBookRepository
     * @param DoctrineUserRepository $doctrineUserRepository
     * @param DoctrineBookUserRepository $doctrineBookUserRepository
     * @param AutoMapperInterface $autoMapper
     */
    public function __construct(DoctrineBookRepository $doctryneBookRepository , DoctrineUserRepository $doctrineUserRepository , DoctrineBookUserRepository $doctrineBookUserRepository ,  AutoMapperInterface $autoMapper)
    {
    	$this->doctryneBookRepository     = $doctryneBookRepository;
        $this->doctrineUserRepository     = $doctrineUserRepository;
        $this->doctrineBookUserRepository = $doctrineBookUserRepository;
    	$this->autoMapper                 = $autoMapper;
    }

    /**
     * @param BookCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \AutoMapperPlus\Exception\UnregisteredMappingException
     * @throws \BookStore\Domain\Exception\RegisterBookException
     */
    public function store(BookCreateRequest $request)
    {
        $data = $this->autoMapper->map($request->all(), BookDto::class);

        $book = (new RegisterService($this->doctryneBookRepository , $this->doctrineUserRepository,$this->doctrineBookUserRepository))->execute($data);

        return response()->json($this->autoMapper->map($book, BookDto::class),201);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \BookStore\Domain\Exception\RegisterBookException
     */
    public function buying(Request $request)
    {
        (new BuyingService($this->doctryneBookRepository , $this->doctrineUserRepository,$this->doctrineBookUserRepository))->execute($request->input('book_id'),$request->input('user_id'),$request->input('quantity'));

        return response()->json('success',201);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \BookStore\Domain\Exception\RegisterBookException
     */
    public function reserving(Request $request)
    {
        (new ReservingService($this->doctryneBookRepository , $this->doctrineUserRepository,$this->doctrineBookUserRepository))->execute($request->input('book_id'),$request->input('user_id'),$request->input('quantity'));

        return response()->json('success',201);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \BookStore\Domain\Exception\RegisterBookException
     */
    public function financing(Request $request)
    {
        (new FinancingService($this->doctryneBookRepository , $this->doctrineUserRepository,$this->doctrineBookUserRepository))->execute($request->input('book_id'),$request->input('user_id'),$request->input('amount'));

        return response()->json('success',201);
    }
}

