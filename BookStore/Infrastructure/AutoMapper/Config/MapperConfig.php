<?php


namespace BookStore\Infrastructure\AutoMapper\Config;

use AutoMapperPlus\DataType;
use BookStore\Domain\Dto\User\UserDto;
use BookStore\Domain\Dto\Book\BookDto;
use BookStore\Domain\Dto\Promotion\PromotionDto;
use BookStore\Domain\Model\User\User;
use BookStore\Domain\Model\Book\Book;
use BookStore\Domain\Model\Promotion\Promotion;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\MappingOperation\Operation;
use AutoMapperPlus\Configuration\AutoMapperConfig;

class MapperConfig extends AutoMapperConfig
{

    /**
     * MapperConfig constructor.
     * @param callable|null $configurator
     */
    public function __construct(callable $configurator=null)
    {
        parent::__construct($configurator);
        $this->getOptions()->createUnregisteredMappings();
        $this->initialize();

    }

    /**
     *
     */
    private function initialize()
    {
        $this->registerMapping(DataType::ARRAY, UserDto::class);

        $this->registerMapping(User::class, UserDto::class);

        $this->registerMapping(DataType::ARRAY, BookDto::class);

        $this->registerMapping(Book::class, BookDto::class);

        $this->registerMapping(DataType::ARRAY, PromotionDto::class);

        $this->registerMapping(Book::class, BookDto::class);
    }

}
