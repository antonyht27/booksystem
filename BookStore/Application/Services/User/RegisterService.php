<?php


namespace BookStore\Application\Services\User;


use BookStore\Domain\Model\User\User;
use BookStore\Domain\Dto\User\UserDto;
use BookStore\Domain\Repositories\User\UserRepository;
use BookStore\Domain\Exception\RegisterUserException;

class RegisterService
{

    protected $userRepository;

    /**
     * Constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param  UserDto $userDto
     * @return User
     */

    public function execute(UserDto $userDto)
    {
        try {
            $user = new User();

            $user->setEmail($userDto->email);
            $user->setFirstName($userDto->firstname);
            $user->setLastName($userDto->lastname);
            $user->setDni($userDto->dni);
            $user->setPassword(bcrypt($userDto->password));
            $user->setCreatedAt(new \DateTime("now"));
            $user->setUpdatedAt(new \DateTime("now"));

            $this->userRepository->save($user);
        } catch (\Exception $e) {
            report($e);
            return $e;
        }

        return $user;

    }

}
