<?php


namespace BookStore\Application\Services\Promotion;


use BookStore\Domain\Model\Promotion\Promotion;
use BookStore\Domain\Dto\Promotion\PromotionDto;
use BookStore\Domain\Repositories\Promotion\PromotionRepository;
use BookStore\Domain\Repositories\Book\BookRepository;
use BookStore\Domain\Exception\RegisterPromotionException;

class RegisterService
{

    protected $promotionRepository;

    protected $bookRepository;

    /**
     * Constructor.
     *
     * @param PromotionRepository $promotionRepository
     * @param BookRepository $bookRepository
     */
    public function __construct(PromotionRepository $promotionRepository , BookRepository $bookRepository )
    {
        $this->promotionRepository = $promotionRepository;
        $this->bookRepository      = $bookRepository;
    }

    /**
     * @param PromotionDto $promotionDto
     * @return Promotion
     * @throws RegisterPromotionException
     */

    public function execute(PromotionDto $promotionDto)
    {
        try {
            $promotion = new Promotion();
            $book = $this->bookRepository->findOneById($promotionDto->book);

            $promotion->setPromotionType($promotionDto->promotionType);

            if($promotionDto->start_date)
            {
                $promotion->setStart($promotionDto->start_date);
            }

            if($promotionDto->end_date)
            {
                $promotion->setEnd($promotionDto->end_date);
            }

            if($promotionDto->discount)
            {
                $promotion->setDiscount($promotionDto->discount);
            }

            if($promotionDto->collected)
            {
                $promotion->setCollected($promotionDto->collected);
            }

            $promotion->setCreatedAt(new \DateTime("now"));
            $promotion->setUpdatedAt(new \DateTime("now"));

            $promotion->setBook($book);

            $this->promotionRepository->save($promotion);

        } catch (\Exception $e) {
            report($e);
            throw new RegisterPromotionException();

        }

        return $promotion;

    }

}
