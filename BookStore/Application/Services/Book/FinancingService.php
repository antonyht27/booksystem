<?php


namespace BookStore\Application\Services\Book;


use Illuminate\Support\Carbon;
use BookStore\Domain\Model\Book\Book;
use BookStore\Domain\Model\Transaction\Transaction;
use BookStore\Domain\Dto\Book\BookDto;
use BookStore\Domain\Model\BookUser\BookUser;
use BookStore\Domain\Model\Promotion\Promotion;
use BookStore\Domain\Exception\RegisterBookException;
use BookStore\Domain\Repositories\Book\BookRepository;
use BookStore\Domain\Repositories\User\UserRepository;
use BookStore\Domain\Repositories\BookUser\BookUserRepository;
use BookStore\Application\Services\Transaction\RegisterService as TransactionRegisterService;

class FinancingService
{
    /**
     * @var BookRepository
     */
    protected $bookRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var BookUserRepository
     */
    protected $bookUserRepository;

    /**
     * Constructor.
     *
     * @param BookRepository $bookRepository
     */
    public function __construct(BookRepository $bookRepository , UserRepository $userRepository , BookUserRepository $bookUserRepository )
    {
        $this->bookRepository        = $bookRepository;
        $this->userRepository        = $userRepository;
        $this->bookUserRepository    = $bookUserRepository;
    }

    /**
     * @param int $book_id
     * @param int $user_id
     * @param float $amount
     * @return BookUser
     * @throws RegisterBookException
     */

    public function execute(int $book_id, int $user_id, float $amount)
    {
        try {

            $bookUser = new BookUser();
            $transaction = new Transaction();
            
            $user = $this->userRepository->findOneById($user_id);

            if(!$user)
            {
                throw new Exception("Error Processing Request", 1);
            }

            $book = $this->bookRepository->findOneById($book_id);

            if(!$book)
            {
                throw new Exception("Error Processing Request", 1);
            }

            $bookUser->setUser($user);
            $bookUser->setBook($book);
            $bookUser->setCreatedAt(new \DateTime("now"));
            $bookUser->setUpdatedAt(new \DateTime("now"));
            $bookUser->setType('Reader');             

            $transaction->setAmount($amount);
            $transaction->setBookUser($bookUser);
            $transaction->setTransactionType('financing');
            $transaction->setCreatedAt(new \DateTime("now"));
            $transaction->setUpdatedAt(new \DateTime("now"));

            $bookUser->addTransaction($transaction);

            $this->bookUserRepository->save($bookUser);

            return $bookUser;

        } catch (\Exception $e) {
            report($e);
            throw new RegisterBookException();
            
        }

        return $bookUser;

    }

}
