<?php


namespace BookStore\Application\Services\Book;


use BookStore\Domain\Model\Book\Book;
use BookStore\Domain\Model\BookUser\BookUser;
use BookStore\Domain\Dto\Book\BookDto;
use BookStore\Domain\Repositories\Book\BookRepository;
use BookStore\Domain\Repositories\User\UserRepository;
use BookStore\Domain\Repositories\BookUser\BookUserRepository;
use BookStore\Domain\Exception\RegisterBookException;

class RegisterService
{

    protected $bookRepository;

    protected $userRepository;

    protected $bookUserRepository;

    /**
     * Constructor.
     *
     * @param BookRepository $bookRepository
     */
    public function __construct(BookRepository $bookRepository , UserRepository $userRepository , BookUserRepository $bookUserRepository )
    {
        $this->bookRepository     = $bookRepository;
        $this->userRepository     = $userRepository;
        $this->bookUserRepository = $bookUserRepository;
    }

    /**
     * @param BookDto $bookDto
     * @return Book
     * @throws RegisterBookException
     */

    public function execute(BookDto $bookDto)
    {
        try {
            $book = new Book();
            $bookUser = new BookUser();
            $user = $this->userRepository->findOneById($bookDto->user);

            $book->setTitle($bookDto->title);
            $book->setPrice($bookDto->price);
            $book->setState($bookDto->state);
            $book->setStock($bookDto->stock);
            $book->setCreatedAt(new \DateTime("now"));
            $book->setUpdatedAt(new \DateTime("now"));

            $bookUser->setUser($user);
            $bookUser->setBook($book);
            $bookUser->setQuantity($bookDto->quantity);
            $bookUser->setCreatedAt(new \DateTime("now"));
            $bookUser->setUpdatedAt(new \DateTime("now"));
            $bookUser->setType('Author');

            $book->addBookUser($bookUser);

            $this->bookRepository->save($book);

            return $book;

        } catch (\Exception $e) {
            report($e);
            throw new RegisterBookException();

        }

        return $book;

    }

}
