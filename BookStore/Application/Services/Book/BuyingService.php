<?php


namespace BookStore\Application\Services\Book;


use Illuminate\Support\Carbon;
use BookStore\Domain\Model\Book\Book;
use BookStore\Domain\Model\Transaction\Transaction;
use BookStore\Domain\Dto\Book\BookDto;
use BookStore\Domain\Model\BookUser\BookUser;
use BookStore\Domain\Model\Promotion\Promotion;
use BookStore\Domain\Exception\RegisterBookException;
use BookStore\Domain\Repositories\Book\BookRepository;
use BookStore\Domain\Repositories\User\UserRepository;
use BookStore\Domain\Repositories\BookUser\BookUserRepository;
use BookStore\Application\Services\Transaction\RegisterService as TransactionRegisterService;

class BuyingService
{

    protected $bookRepository;

    protected $userRepository;

    protected $bookUserRepository;

    /**
     * Constructor.
     *
     * @param BookRepository $bookRepository
     */
    public function __construct(BookRepository $bookRepository , UserRepository $userRepository , BookUserRepository $bookUserRepository )
    {
        $this->bookRepository        = $bookRepository;
        $this->userRepository        = $userRepository;
        $this->bookUserRepository    = $bookUserRepository;
    }

    /**
     * @param int $book_id
     * @param int $user_id
     * @param int $quantity
     * @return BookUser
     * @throws RegisterBookException
     */

    public function execute(int $book_id, int $user_id, int $quantity)
    {
        try {

            $bookUser = new BookUser();
            $transaction = new Transaction();

            $user = $this->userRepository->findOneById($user_id);

            if(!$user)
            {
                throw new Exception("Error Processing Request", 1);
            }

            $book = $this->bookRepository->findOneById($book_id);

            if(!$book)
            {
                throw new Exception("Error Processing Request", 1);
            }

            $bookUser->setUser($user);
            $bookUser->setBook($book);
            $bookUser->setQuantity($quantity);
            $bookUser->setCreatedAt(new \DateTime("now"));
            $bookUser->setUpdatedAt(new \DateTime("now"));
            $bookUser->setType('Reader');

            $promotion = $book->getPromotions()->filter(function(Promotion $promotion){
                return $promotion->getPromotionType() === 'promotion';
            })->first();

            if($promotion)
            {
                if(Carbon::parse(new \DateTime("now"))->greaterThanOrEqualTo($promotion->getStart()) && Carbon::parse(new \DateTime("now"))->lessThanOrEqualTo($promotion->getEnd()) )
                {
                    $bookAmount = ( $book->getPrice() * $quantity ) - ( ( $book->getPrice() * $quantity ) * $promotion->getDiscount() );
                }
            }
            else
            {
                $bookAmount = $book->getPrice() * $quantity;
            }

            $transaction->setAmount($bookAmount);
            $transaction->setBookUser($bookUser);
            $transaction->setTransactionType('buying');
            $transaction->setCreatedAt(new \DateTime("now"));
            $transaction->setUpdatedAt(new \DateTime("now"));

            $bookUser->addTransaction($transaction);

            $this->bookUserRepository->save($bookUser);

            return $bookUser;

        } catch (\Exception $e) {
            report($e);
            throw new RegisterBookException();

        }

        return $bookUser;

    }

}
