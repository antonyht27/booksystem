<?php


namespace BookStore\Domain\Repositories\Transaction;


use BookStore\Domain\Model\Book\Transaction;

interface TransactionRepository
{
    /**
     * @param Transaction $transaction
     */
   public function save(Transaction $transaction): void;

    /**
     * @param int $id
     * @return Transaction|null
     */
   public function findOneById(int $id): ?Transaction;

}
