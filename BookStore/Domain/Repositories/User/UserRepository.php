<?php


namespace BookStore\Domain\Repositories\User;


use BookStore\Domain\Model\User\User;

interface UserRepository
{
    /**
     * @param User $user
     */
   public function save(User $user): void;

    /**
     * @param int $id
     * @return User|null
     */
   public function findOneById(int $id): ?User;

}
