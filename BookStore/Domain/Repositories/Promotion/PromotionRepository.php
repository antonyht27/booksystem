<?php


namespace BookStore\Domain\Repositories\Promotion;


use BookStore\Domain\Model\Promotion\Promotion;

interface PromotionRepository
{
    /**
     * @param Promotion $promotion
     */
   public function save(Promotion $promotion): void;

    /**
     * @param int $id
     * @return Promotion|null
     */
   public function findOneById(int $id): ?Promotion;

}
