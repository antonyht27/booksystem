<?php


namespace BookStore\Domain\Repositories\Book;


use BookStore\Domain\Model\Book\Book;

interface BookRepository
{
    /**
     * @param Book $book
     */
   public function save(Book $book): void;

    /**
     * @param int $id
     * @return Book|null
     */
   public function findOneById(int $id): ?Book;

}
