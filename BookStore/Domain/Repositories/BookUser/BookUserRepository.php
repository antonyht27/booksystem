<?php


namespace BookStore\Domain\Repositories\BookUser;


use BookStore\Domain\Model\BookUser\BookUser;

interface BookUserRepository
{
    /**
     * @param BookUser $bookUser
     */
   public function save(BookUser $bookUser): void;

    /**
     * @param int $id
     * @return BookUser|null
     */
   public function findOneById(int $id): ?BookUser;

}
