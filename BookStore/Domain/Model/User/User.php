<?php

namespace BookStore\Domain\Model\User;

use Doctrine\ORM\Mapping as ORM;
use BookStore\Domain\Model\Book\Book;
use Doctrine\Common\Collections\ArrayCollection;
use BookStore\Domain\Model\Transaction\Transaction;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->books        = new ArrayCollection();
        $this->transactions = new ArrayCollection();
        $this->roles        = new ArrayCollection();

    }

     /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="email", nullable=false)
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string", name="password", nullable=false)
     */
    protected $password;

    /**
     * @var string
     * @ORM\Column(type="string", name="dni", nullable=false)
     */
    protected $dni;

    /**
     * @var string
     * @ORM\Column(type="string", name="firstname", nullable=false)
     */
    protected $firstname;

    /**
     * @var string
     * @ORM\Column(type="string", name="lastname", nullable=false)
     */
    protected $lastname;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    protected $createdAt;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    protected $updatedAt;

    /**
    * @var ArrayCollection
    * @ORM\OneToMany(targetEntity="BookStore\Domain\Model\BookUser\BookUser", mappedBy="user", cascade={"all"})
    */
    protected $books_users;

    /**
     * @var ArrayCollection
     * One User have Many Transactions.
     * @ORM\OneToMany(targetEntity="BookStore\Domain\Model\Transaction\Transaction", mappedBy="User",cascade={"all"})
     */
    protected $transactions;

    /**
    * @var ArrayCollection
     * One Users have Many Roles.
     * @ORM\ManyToMany(targetEntity="BookStore\Domain\Model\Role\Role", inversedBy="users")
     * @ORM\JoinTable(name="role_user")
     */
    protected $roles;



    /**
     * Get the value of id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;

    }


    /**
     * Set the value of id
     *
     * @param integer $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * Get the value of email
     *
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;

    }

    /**
     * Set the value of email
     *
     * @param string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;

    }

    /**
     * Get the value of password
     *
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;

    }

    /**
     * Set the value of password
     *
     * @param string|null $password
     */
    public function setPassword(?string $password): void
    {
        $this->password = $password;

    }

    /**
     * Get the value of dni
     *
     * @return string
     */
    public function getDni(): ?string
    {
        return $this->dni;

    }

    /**
     * Set the value of dni
     *
     * @param string|null $dni
     */
    public function setDni(?string $dni): void
    {
        $this->dni = $dni;

    }

    /**
     * Get the value of firstname
     *
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstname;

    }

    /**
     * Set the value of firstname
     *
     * @param string $firstname
     */
    public function setFirstName(?string $firstname): void
    {
        $this->firstname = $firstname;

    }

    /**
     * Get the value of lastname
     *
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastname;

    }

    /**
     * Set the value of lastname
     *
     * @param string $lastname
     */
    public function setLastName(?string $lastname): void
    {
        $this->lastname = $lastname;

    }

    /**
     * Get the value of createdAt
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;

    }

    /**
     * Set the value of createdAt
     *
     * @param \DateTime|null $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;

    }

    /**
     * Get the value of updatedAt
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->createdAt;

    }

    /**
     * Set the value of updatedAt
     *
     * @param string $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;

    }

    /**
     * @return Array|null
     */
    public function getBooKUser(): ?Array
    {
        return $this->books_users;
    }

    /**
     * @param BooKUser|null $bookUser
     */
    public function addBooKUser(?BooKUser $bookUser): void
    {
        if(!$this->books_users->contains($bookUser)) {
            $bookUser->setUser($this);
            $this->books_users->add($bookUser);
        }
    }

    /**
     * Get array of Roles related with User
     *
     * @return array|null
     */
    public function getRoles(): ?array
    {
        return $this->roles;

    }

    /**
     * Add new book related with User
     *
     * @param Role|null $role
     */
    public function addRole(?Role $role): void
    {
        if(!$this->roles->contains($role)) {
            $role->addUser($this);
            $this->roles->add($role);
        }
    }

}
