<?php

namespace BookStore\Domain\Model\Transaction;

use Doctrine\ORM\Mapping as ORM;
use BookStore\Domain\Model\BookUser\BookUser;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="transactions")
 */
class Transaction
{
    public function __construct()
    {

    }

     /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var decimal
     * @ORM\Column(type="decimal", name="amount", nullable=false)
     */
    protected $amount;

    /**
     * @var string
     * @ORM\Column(type="string", name="type", nullable=false)
     */
    protected $transactionType;

    /**
     * @var BookUser
     @ORM\ManyToOne(targetEntity="BookStore\Domain\Model\BookUser\BookUser", inversedBy="transactions")
     */
    protected $bookUser;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    protected $createdAt;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    protected $updatedAt;

    /**
     * Get the value of id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;

    }


    /**
     * Set the value of id
     *
     * @param integer $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;

    }

    /**
     * Get the value of amount
     *
     * @return float
     */
    public function getAmount(): ?float
    {
        return $this->amount;

    }

    /**
     * Set the value of amount
     *
     * @param float $amount
     */
    public function setAmount(?float $amount): void
    {
        $this->amount = $amount;

    }

    /**
     * Get the value of transactionType
     *
     * @return string
     */
    public function getTransactionType(): ?string
    {
        return $this->transactionType;

    }

    /**
     * Set the value of transactionType
     *
     * @param string $transactionType
     */
    public function setTransactionType(?string $transactionType): void
    {
        $this->transactionType = $transactionType;

    }

    /**
     * Get the value of createdAt
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;

    }

    /**
     * Set the value of createdAt
     *
     * @param \DateTime|null $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;

    }

    /**
     * Get the value of updatedAt
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;

    }

    /**
     * Set the value of updatedAt
     *
     * @param \DateTime|null $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;

    }

    /**
     * @return BookUser|null
     */
    public function getBookUser(): ? BookUser
    {
        return $this->bookUser;
    }

    /**
     * @param BookUser|null $bookUser
     */
    public function setBookUser(?BookUser $bookUser): void
    {
        $this->bookUser = $bookUser;
    }


}
