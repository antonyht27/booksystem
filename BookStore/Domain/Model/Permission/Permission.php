<?php

namespace BookStore\Domain\Model\Permission;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="permissions")
 */
class Permission
{
    public function __construct()
    {
        $this->roles = new ArrayCollection();

    }

     /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="name", nullable=false)
     */
    protected $name;

    /**
     * @var ArrayCollection
     * Many Permissions have Many Roles.
     * @ORM\ManyToMany(targetEntity="BookStore\Domain\Model\Role\Role", mappedBy="permissions")
     * @ORM\JoinTable(name="permision_role")
     */
    protected $roles;

     /**
     * @var datetime
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    protected $createdAt;

    /**
     * Get the value of id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;

    }


    /**
     * Set the value of id
     *
     * @param integer $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;

    }

    /**
     * Get the value of name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;

    }

    /**
     * Set the value of name
     *
     * @param string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;

    }

    /**
     * Get the value of createdAt
     *
     * @return datetime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;

    }

    /**
     * Set the value of createdAt
     *
     * @param string $createdAt
     */
    public function setCreatedAt(?DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;

    }

    /**
     * Get array of Roles related with Permission
     *
     * @return Array
     */
    public function getRoles(): ?array
    {
        return $this->roles;

    }

    /**
     * Add new Role related with Permission
     *
     * @param Role|null $role
     */
    public function addRole(?Role $role): void
    {
        if(!$this->roles->contains($role)) {
            $role->addPermission($this);
            $this->reols->add($role);
        }
    }

}
