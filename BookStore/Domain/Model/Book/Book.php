<?php

namespace BookStore\Domain\Model\Book;

use Doctrine\ORM\Mapping as ORM;
use BookStore\Domain\Model\User\User;
use BookStore\Domain\Model\Promotion\Promotion;
use BookStore\Domain\Model\BookUser\BookUser;;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="books")
 */
class Book
{

    public function __construct()
    {
        $this->users       = new ArrayCollection();
        $this->promotions  = new ArrayCollection();
        $this->books_users = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="title", nullable=false)
     */
    protected $title;

    /**
     * @var decimal
     * @ORM\Column(type="decimal", name="price", nullable=false)
     */
    protected $price;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="state", nullable=false)
     */
    protected $state;

    /**
     * @var integer
     * @ORM\Column(type="integer", name="stock", nullable=false)
     */
    protected $stock;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    protected $createdAt;

     /**
     * @var datetime
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    protected $updatedAt;

    /**
    * @var ArrayCollection
    * @ORM\OneToMany(targetEntity="BookStore\Domain\Model\BookUser\BookUser", mappedBy="Book", cascade={"all"})
    */
    protected $books_users;

    /**
     * @var ArrayCollection
     * One Book have Many Promotions.
     * @ORM\OneToMany(targetEntity="BookStore\Domain\Model\Promotion\Promotion", mappedBy="book")
     */
    protected $promotions;

    /**
     * Get the value of id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;

    }


    /**
     * Set the value of id
     *
     * @param integer $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;

    }

    /**
     * Get the value of title
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;

    }

    /**
     * Set the value of title
     *
     * @param string $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;

    }

    /**
     * Get the value of price
     *
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;

    }

    /**
     * Set the value of price
     *
     * @param float $price
     */
    public function setPrice(?float $price): void
    {
        $this->price = $price;

    }

     /**
     * Get the value of state
     *
     * @return bool
     */
    public function getState(): ?bool
    {
        return $this->state;

    }

    /**
     * Set the value of state
     *
     * @param bool $state
     */
    public function setState(?bool $state): void
    {
        $this->state = $state;

    }

    /**
     * Get the value of stock
     *
     * @return integer
     */
    public function getStock(): ?int
    {
        return $this->stock;

    }

    /**
     * Set the value of stock
     *
     * @param integer $stock
     */
    public function setStock(?int $stock): void
    {
        $this->stock = $stock;

    }

    /**
     * Get the value of createdAt
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;

    }

    /**
     * Set the value of createdAt
     *
     * @param \DateTime|null $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;

    }

    /**
     * Get the value of updatedAt
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;

    }

    /**
     * Set the value of updatedAt
     *
     * @param \DateTime|null $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;

    }

    public function getBooKUser(): ?Array
    {
        return $this->books_users;
    }

    public function addBooKUser(?BooKUser $bookUser): void
    {
        if(!$this->books_users->contains($bookUser)) {
            $bookUser->setBook($this);
            $this->books_users->add($bookUser);
        }
    }

    /**
     * Get array of promotions related with Book
     *
     * @return ArrayCollection
     */
    public function getPromotions()
    {
        return $this->promotions;

    }

    /**
     * Add new Promotion related with Book
     *
     * @param Promotion $promotion
     */
    public function addPromotion(?Promotion $promotion): void
    {
        if(!$this->promotions->contains($promotion)) {
            $promotion->addBook($this);
            $this->promotions->add($promotion);
        }
    }


}
