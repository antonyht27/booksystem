<?php

namespace BookStore\Domain\Model\Role;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="roles")
 */
class Role
{
    public function __construct()
    {
        $this->users        = new ArrayCollection();
        $this->permissions  = new ArrayCollection();

    }

     /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="name", nullable=false)
     */
    protected $name;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    protected $createdAt;

    /**
     * @var ArrayCollection
     * Many Roles have Many Permissions.
     * @ORM\ManyToMany(targetEntity="BookStore\Domain\Model\Permission\Permission", mappedBy="roles")
     * @ORM\JoinTable(name="permision_role")
     */
    protected $permissions;

    /**
    * @var ArrayCollection
     * Many Roles have Many Permissions.
     * @ORM\ManyToMany(targetEntity="BookStore\Domain\Model\User\User", inversedBy="roles")
     * @ORM\JoinTable(name="role_user")
     */
    protected $users;



    /**
     * Get the value of id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;

    }


    /**
     * Set the value of id
     *
     * @param integer $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;

    }

    /**
     * Get the value of name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;

    }

    /**
     * Set the value of name
     *
     * @param string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;

    }

    /**
     * Get the value of createdAt
     *
     * @return datetime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;

    }

    /**
     * Set the value of createdAt
     *
     * @param DateTime|null $createdAt
     */
    public function setCreatedAt(?DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;

    }

    /**
     * Get array of Permissions related with Role
     *
     * @return array|null
     */
    public function getPermissions(): ?array
    {
        return $this->permissions;

    }

    /**
     * Add new Permission related with Role
     *
     * @param Permission $permission
     */
    public function addPermission(?Permission $permission): void
    {
        if(!$this->permissions->contains($permission)) {
            $permission->addRole($this);
            $this->permissions->add($permission);
        }
    }

    /**
     * Get array of Users related with Role
     *
     * @return array|null
     */
    public function getUsers(): ?array
    {
        return $this->users;

    }

    /**
     * Add new User related with Role
     *
     * @param User $user
     */
    public function addUser(?User $user): void
    {
        if(!$this->users->contains($user)) {
            $user->addRole($this);
            $this->users->add($user);
        }
    }

}
