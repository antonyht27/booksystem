<?php

namespace BookStore\Domain\Model\BookUser;

use Doctrine\ORM\Mapping as ORM;
use BookStore\Domain\Model\Book\Book;
use BookStore\Domain\Model\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use BookStore\Domain\Model\Transaction\Transaction;

/**
 * @ORM\Entity
 * @ORM\Table(name="book_user")
 */
class BookUser
{
    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

     /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="type", nullable=false)
     */
    protected $type;

    /**
     * @var integer
     * @ORM\Column(type="integer", name="quantity", nullable=false)
     */
    protected $quantity;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    protected $createdAt;

     /**
     * @var datetime
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    protected $updatedAt;

    /**
     * @var BookStore\Domain\Model\Book\Book
     * @ORM\ManyToOne(targetEntity="BookStore\Domain\Model\Book\Book", inversedBy="books_users")
     * @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     */
    protected $book;

    /**
     * @var BookStore\Domain\Model\User\User
     * @ORM\ManyToOne(targetEntity="BookStore\Domain\Model\User\User", inversedBy="books_users")
     */
    protected $user;

    /**
    * @var ArrayCollection
    * @ORM\OneToMany(targetEntity="BookStore\Domain\Model\Transaction\Transaction", mappedBy="bookUser", cascade={"all"})
    */
    protected $transactions;

    /**
     * Get the value of id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;

    }


    /**
     * Set the value of id
     *
     * @param integer $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;

    }

    /**
     * Get the value of type
     *
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;

    }

    /**
     * Set the value of type
     *
     * @param string $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;

    }

    /**
     * Get the value of quantity
     *
     * @return integer
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;

    }

    /**
     * Set the value of quantity
     *
     * @param float $quantity
     */
    public function setQuantity(?int $quantity): void
    {
        $this->quantity = $quantity;

    }

    /**
     * Get the value of createdAt
     *
     * @return datetime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;

    }

    /**
     * Set the value of createdAt
     *
     * @param string $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;

    }

    /**
     * Get the value of updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;

    }

    /**
     * Set the value of updatedAt
     *
     * @param string $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;

    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Book|null
     */
    public function getBook(): ?Book
    {
        return $this->book;
    }

    /**
     * @param Book|null $book
     */
    public function setBook(?Book $book): void
    {
        $this->book = $book;
    }

    /**
     * @param Transaction $transaction
     */
    public function addTransaction(Transaction $transaction): void
    {
        if(!$this->transactions->contains($transaction)) {
            $transaction->setBookUser($this);
            $this->transactions->add($transaction);
        }
    }

    /**
     * @return Array|null
     */
    public function getTransaction(): ?Array
    {
        return $this->transactions;
    }

}
