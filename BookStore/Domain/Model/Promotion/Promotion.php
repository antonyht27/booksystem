<?php

namespace BookStore\Domain\Model\Promotion;

use Doctrine\ORM\Mapping as ORM;
use BookStore\Domain\Model\Book\Book;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="promotions")
 */
class Promotion
{
    public function __construct()
    {

    }

     /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="start_date", nullable=true)
     */
    protected $start_date;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="end_date", nullable=true)
     */
    protected $end_date;

     /**
     * @var string
     * @ORM\Column(type="string", name="type", nullable=false)
     */
    protected $promotionType;

    /**
     * @var integer
     * @ORM\Column(type="integer", name="discount", nullable=true)
     */
    protected $discount;

    /**
     * @var decimal
     * @ORM\Column(type="decimal", name="collected", nullable=true)
     */
    protected $collected;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    protected $createdAt;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    protected $updatedAt;

     /**
     * @var Book
     * Many Promotions have one Book.
     * @ORM\ManyToOne(targetEntity="BookStore\Domain\Model\Book\Book", inversedBy="promotions")
     */
    protected $book;

    /**
     * Get the value of id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;

    }


    /**
     * Set the value of id
     *
     * @param integer $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;

    }

    /**
     * Get the value of start
     *
     * @return \DateTime|null
     */
    public function getStart(): ?\DateTime
    {
        return $this->start_date;

    }

    /**
     * Set the value of start
     *
     * @param \DateTime|null $start_date
     */
    public function setStart(?\DateTime $start_date): void
    {
        $this->start_date = $start_date;

    }

    /**
     * Get the value of end
     *
     * @return datetime
     */
    public function getEnd(): ?\DateTime
    {
        return $this->end_date;

    }

    /**
     * Set the value of end
     *
     * @param \DateTime|null $end_date
     */
    public function setEnd(?\DateTime $end_date): void
    {
        $this->end_date = $end_date;

    }

    /**
     * Get the value of promotionType
     *
     * @return string
     */
    public function getPromotionType(): ?string
    {
        return $this->promotionType;

    }

    /**
     * Set the value of promotionType
     *
     * @param string $promotionType
     */
    public function setPromotionType(?string $promotionType): void
    {
        $this->promotionType = $promotionType;

    }

    /**
     * Get the value of discount
     *
     * @return integer
     */
    public function getDiscount(): ?int
    {
        return $this->discount;

    }

    /**
     * Set the value of discount
     *
     * @param integer $discount
     */
    public function setDiscount(?int $discount): void
    {
        $this->discount = $discount;

    }

     /**
     * Get the value of collecte
     *
     * @return float
     */
    public function getCollected(): ?float
    {
        return $this->collected;

    }

    /**
     * Set the value of collected
     *
     * @param float|null $collected
     */
    public function setCollected(?float $collected): void
    {
        $this->collected = $collected;

    }

    /**
     * Get the value of createdAt
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;

    }

    /**
     * Set the value of createdAt
     *
     * @param \DateTime|null $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;

    }

    /**
     * Get the value of updatedAt
     *
     * @return \DateTime|null
     */
    public function getupdatedAt(): ?\DateTime
    {
        return $this->updatedAt;

    }

    /**
     * Set the value of updatedAt
     *
     * @param \DateTime|null $updatedAt
     */
    public function setupdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;

    }

    /**
     * Get the book related with Promotion
     *
     * @return Book
     */
    public function getBook(): ?Book
    {
        return $this->book;

    }

    /**
     * Set the Book related with Promotion
     *
     * @param Book $book
     */
    public function setBook(?Book $book): void
    {
        $this->book = $book;

    }


}
