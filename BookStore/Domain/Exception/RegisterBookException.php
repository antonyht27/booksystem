<?php


namespace BookStore\Domain\Exception;

use Exception;


class RegisterBookException extends Exception
{

    public function report()
    {

    }

    public function render($request)
    {
        return response()->json(
            [
                'message' => 'Ocurrio un error al intentar registrar el libro',
            ],
            400
        );

    }

}
