<?php


namespace BookStore\Domain\Exception;

use Exception;


class RegisterUserException extends Exception 
{

    public function report()
    {

    }

    public function render($request)
    {
        return response()->json(
            [
                'message' => 'Ocurrio un error al intentar registrar el usuario',
            ],
            400
        );

    }

}