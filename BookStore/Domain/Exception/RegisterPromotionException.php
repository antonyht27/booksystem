<?php


namespace BookStore\Domain\Exception;

use Exception;


class RegisterPromotionException extends Exception
{

    public function report()
    {

    }

    public function render($request)
    {
        return response()->json(
            [
                'message' => 'Ocurrio un error al intentar registrar la promocion',
            ],
            400
        );

    }

}
