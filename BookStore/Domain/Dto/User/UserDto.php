<?php

namespace BookStore\Domain\Dto\User;

class UserDto
{

    /**
     * @var string $email
     */
    public $email;

    /**
     * @var string $firstName
     */
    public $firstname;

    /**
     * @var string $lastName
     */
    public $lastname;

    /**
     * @var string $lastName
     */
    public $password;

    /**
     * @var string $dni
     */
    public $dni;

    /**
     * @var datetime $createdAt
     */
    public $createdAt;

    /**
     * @var datetime $updatedAt
     */
    public $updatedAt;

}
