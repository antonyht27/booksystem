<?php

namespace BookStore\Domain\Dto\BookUserDto;

class BookUserDto
{

    /**
     * @var string $type
     */
    public $type;

    /**
     * @var integer $quantity
     */
    public $quantity;

    /**
     * @var datetime $createdAt
     */
    public $createdAt;

    /**
     * @var datetime $updatedAt
     */
    public $updatedAt;

    /**
     * @var Book $book
     */
    public $book;

    /**
     * @var User $user
     */
    public $user;

}
