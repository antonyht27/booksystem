<?php

namespace BookStore\Domain\Dto\Promotion;

class PromotionDto
{

    /**
     * @var string $promotionType
     */
    public $promotionType;

    /**
     * @var integer $discount
     */
    public $discount;

    /**
     * @var datetime $start_date
     */
    public $start_date;

    /**
     * @var datetime $end_date
     */
    public $end_date;

    /**
     * @var float $collected
     */
    public $collected;

    /**
     * @var Book $book
     */
    public $book;

    /**
     * @var datetime $createdAt
     */
    public $createdAt;

    /**
     * @var datetime $updatedAt
     */
    public $updatedAt;

}
