<?php

namespace BookStore\Domain\Dto\Book;

class BookDto
{

    /**
     * @var string $title
     */
    public $title;

    /**
     * @var float $price
     */
    public $price;

    /**
     * @var bool $state
     */
    public $state;

    /**
     * @var integer $stock
     */
    public $stock;

     /**
     * @var User $user
     */
    public $user;

    /**
     * @var integer $quantity
     */
    public $quantity;

}
