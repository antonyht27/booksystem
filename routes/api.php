<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'User','prefix'=> 'user'],function () {
    Route::get('', 'UserController@getAll');
    Route::get('/balance', 'UserController@balance');
    Route::post('/store', 'UserController@store');
}
);

Route::group(['namespace'=>'Book','prefix'=>'book'],function () {
    Route::get('', 'BookController@getAll');
    Route::post('/store', 'BookController@store');
    Route::post('/buying', 'BookController@buying');
    Route::post('/reserving', 'BookController@reserving');
    Route::post('/financing', 'BookController@financing');
}
);

Route::group(['namespace'=>'Promotion','prefix'=>'promotion',],function () {
    Route::get('', 'PromotionController@getAll');
    Route::post('/store', 'PromotionController@store');
}
);