<?php

namespace Database\Migrations;

use Doctrine\DBAL\Schema\Schema as Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200309124019 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('CREATE TABLE users (  id INT AUTO_INCREMENT NOT NULL,
                                            email VARCHAR(255) NOT NULL,
                                            password VARCHAR(255) NOT NULL,
                                            firstname VARCHAR(255),
                                            lastname VARCHAR(255),
                                            dni VARCHAR(255),
                                            created_at DATETIME NOT NULL,
                                            updated_at DATETIME NOT NULL,
                                            PRIMARY KEY(id), UNIQUE (email))
                                            DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE books (  id INT AUTO_INCREMENT NOT NULL,
                                            title VARCHAR(255) NOT NULL,
                                            price DECIMAL(10, 4) NOT NULL,
                                            state BOOLEAN NOT NULL DEFAULT 0,
                                            stock INT NOT NULL DEFAULT 0,
                                            created_at DATETIME NOT NULL,
                                            updated_at DATETIME NOT NULL,
                                            PRIMARY KEY(id))
                                            DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE promotions ( id INT AUTO_INCREMENT NOT NULL,
                                                type VARCHAR(255) NOT NULL,
                                                discount DECIMAL(10, 2),
                                                start_date DATETIME,
                                                end_date DATETIME,
                                                collected DECIMAL(10, 2),
                                                created_at DATETIME NOT NULL,
                                                updated_at DATETIME NOT NULL,
                                                book_id INT NOT NULL,
                                                INDEX IDEX_BOOK_PROMOTION (book_id),
                                                PRIMARY KEY(id))
                                                DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE book_user ( id INT AUTO_INCREMENT NOT NULL,
                                                type VARCHAR(255) NOT NULL,
                                                created_at DATETIME NOT NULL,
                                                updated_at DATETIME NOT NULL,
                                                book_id INT NOT NULL,
                                                user_id INT NOT NULL,
                                                quantity INT,
                                                INDEX IDEX_BOOK_USER (book_id),
                                                INDEX IDEX_USER_BOOK (user_id),
                                                PRIMARY KEY(id))
                                                DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE transactions (   id INT AUTO_INCREMENT NOT NULL,
                                                    amount DECIMAL(10, 2) NOT NULL,
                                                    type VARCHAR(255) NOT NULL,
                                                    created_at DATETIME NOT NULL,
                                                    updated_at DATETIME NOT NULL,
                                                    book_user_id INT NOT NULL,
                                                    INDEX IDEX_BOOK_USER_TRANSACT (book_user_id),
                                                    PRIMARY KEY(id))
                                                    DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');

        $this->addSql('ALTER TABLE promotions ADD CONSTRAINT FOREINGK_BOOK_PROMO FOREIGN KEY (book_id) REFERENCES books (id)');
        $this->addSql('ALTER TABLE book_user ADD CONSTRAINT FOREINGK_BOOK_USER FOREIGN KEY (book_id) REFERENCES books (id)');
        $this->addSql('ALTER TABLE book_user ADD CONSTRAINT FOREINGK_USER_BOOK FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE transactions ADD CONSTRAINT FOREINGK_BOOK_USER_TRANSACT FOREIGN KEY (book_user_id) REFERENCES book_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE promotions DROP FOREIGN KEY FOREINGK_BOOK_PROMO');
        $this->addSql('ALTER TABLE book_user DROP FOREIGN KEY FOREINGK_BOOK_USER');
        $this->addSql('ALTER TABLE book_user DROP FOREIGN KEY FOREINGK_USER_BOOK');
        $this->addSql('ALTER TABLE transactions DROP FOREIGN KEY FOREINGK_BOOK_USER_TRANSACT');
        $this->addSql('ALTER TABLE transactions DROP FOREIGN KEY FOREINGK_USER_BOOK_TRANSACT');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE books');
        $this->addSql('DROP TABLE promotions');
        $this->addSql('DROP TABLE book_user');
        $this->addSql('DROP TABLE transactions');
    }
}
