<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use BookStore\Infrastructure\Repositories\Book\DoctrineBookRepository;
class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $mytime = Carbon::now();
        $test = DB::table('books')->insert([
            'title' => Str::random(10),
            'price' => mt_rand(1, 100),
            'state' => True,
            'stock' => random_int(1, 999),
            'created_at' => $mytime->toDateTimeString(),
            'updated_at' => $mytime->toDateTimeString()
        ]);
        error_log($test);

        DB::table('promotions')->insert([
            'type' => 'promotion',
            'discount' => mt_rand(1, 100),
            'collected' => random_int(1, 999),
            'start_date' => $mytime->toDateTimeString(),
            'end_date' => $mytime->toDateTimeString(),
            'created_at' => $mytime->toDateTimeString(),
            'updated_at' => $mytime->toDateTimeString(),
            'book_id' => 1
        ]);
    }
}
