<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mytime = Carbon::now();
        factory('BookStore\Domain\Model\User\User')->make();
        //factory('BookStore\Domain\Model\User\User', 10)->create();
            DB::table('users')->insert([
                'firstname' => Str::random(10),
                'lastname' => Str::random(10),
                'dni' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('password'),
                'created_at' => $mytime->toDateTimeString(),
                'updated_at' => $mytime->toDateTimeString()
            ]);
    }
}
